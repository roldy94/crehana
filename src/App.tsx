/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable array-callback-return */
import React, { useEffect } from 'react';
import { CountriesList, CountriesDetails } from './components';
import { Spinner } from './commons';
import { useCountriesQuery } from './generated/graphql';
import { Routes, Route } from "react-router-dom";
import './App.css';

const App = () => {
  const { data, error, loading } = useCountriesQuery();


  useEffect(() => {
    const tickets = [25, 25, 50, 50, 100];
    let ventas = 0;
    let cambio = 0;
    tickets.map((item) => {
      if (item === 25) {
        ventas += item;
      }
      cambio = item - 25;
      ventas = ventas - cambio;      
    });
    if (ventas >= 0) {
      console.log('SI');
    } else {
      console.log('NO');
    }

    // eslint-disable-next-line
  }, []);
  if (loading) {
    return (
      <Spinner />
    )
  }

  if (error || !data) {
    return <div>ERROR</div>;
  }
  return (
    <Routes>
      <Route path="/" element={<CountriesList data={data} />} />
      <Route path="details/:code" element={<CountriesDetails data={data} />} />
    </Routes>
  );
}

export default App;
