import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

const Spinner = () => (
    <Box sx={{
        position: 'fixed',
        width: '2rem',
        height: '2rem',
        zIndex: '999',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        overflow: 'show',
        margin: 'auto',
    }}>
        <CircularProgress />
    </Box>
)

export default Spinner;