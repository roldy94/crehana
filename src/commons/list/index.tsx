import React, { FC } from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { CountriesQuery } from '../../generated/graphql';
import { useNavigate } from "react-router-dom";

interface Props {
    list?: CountriesQuery | undefined;
}

const List: FC<Props> = ({ list }) => {
    const navigate = useNavigate();
    return (
        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {!!list?.countries &&
                list?.countries.map(
                    (country, i) =>
                        !!country && (
                            <Grid item xs={2} sm={4} md={4} key={i}>
                                <Card sx={{ minWidth: 275 }}>
                                    <CardContent sx={{ background: '#007fff' }}>
                                        <Typography sx={{ color: '#ffffff' }} variant="h6" gutterBottom component="div">
                                            {country.name}
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button onClick={() => navigate(`/details/${country.code}`, { replace: true })} size="small">Details</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ),
                )}
        </Grid>
    )
}

export default List;