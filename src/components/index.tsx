import CountriesDetails from './countriesDetails';
import CountriesList from './countriesList';

export {
    CountriesDetails,
    CountriesList
}