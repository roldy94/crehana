import { gql } from '@apollo/client';

export const QUERY_COUNTRIES = gql`
 query Countries {
    countries {
      code
      name
      currency
      continent{
        code
        name
      }
      languages{
        name
        native
      }
      capital
    }
  }  
`;

export const QUERY_COUNTRIES_FILTER = gql`
  query CountriesFilter($filter: CountryFilterInput!) {
    countries(filter: $filter) {
      code
      name
      currency
      continent {
        code
        name
      }
      languages {
        name
        native
      }
      capital
    }
  }
`;

export const QUERY_CONTINENTS = gql`
  query Continents {
    continents{
      code
      name
    }
  }
`;

export const QUERY_COUNTRIES_CURRENCY = gql`
  query Currency {
    countries{
      currency
    }
  }
`;