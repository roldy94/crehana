/* eslint-disable no-const-assign */
import React, { FC, useEffect, useState } from 'react';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { CountriesQuery } from '../../generated/graphql';
import InputLabel from '@mui/material/InputLabel';
import SearchIcon from '@mui/icons-material/Search';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import { Spinner } from '../../commons';
import Select, { SelectChangeEvent } from '@mui/material/Select'
import { useContinentsQuery, useCountriesFilterQuery, CountryFilterInput, useCurrencyQuery } from '../../generated/graphql';
import { useNavigate } from "react-router-dom";

interface Props {
    data: CountriesQuery;
}


const CountriesList: FC<Props> = ({ data }) => {

    const navigate = useNavigate();
    const [code, setCode] = useState('');
    const [textCurrency, setTextCurrency] = useState('');
    const [text, setText] = useState('');
    const [list, setList] = useState<CountriesQuery>();
    const [filter, setFilter] = useState<CountryFilterInput>({});
    const { data: dataSelect } = useContinentsQuery();
    const { data: dataCurrency } = useCurrencyQuery();
    const { data: dataFilter, loading } = useCountriesFilterQuery({ variables: { filter: filter } });


    useEffect(() => {
        if (dataFilter?.countries.length !== 0) {
            setList(dataFilter)
        } else {
            setList(data);
        }
        // eslint-disable-next-line
    }, [dataFilter]);


    const handleChangeSelect = (event: SelectChangeEvent) => {
        setCode(event.target.value as string);
        setFilter({ continent: { eq: event.target.value as string } })
    };

    const handleChangeSelectCurrency = (event: SelectChangeEvent) => {
        setTextCurrency(event.target.value as string);
        setFilter({ currency: { eq: event.target.value as string } })
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setText(event.target.value as string);
    };

    if (loading) {
        return (<Spinner />)
    }

    return (
        <Container sx={{ padding: '1rem' }}>

            <FormControl fullWidth sx={{ marginBottom: '1rem' }}>
                <InputLabel id="label-currency">Currency</InputLabel>
                <Select
                    labelId="label-currency"
                    id="select-currency"
                    value={textCurrency}
                    label="currency"
                    onChange={handleChangeSelectCurrency}
                >
                    {!!dataCurrency?.countries && dataCurrency?.countries.map((country, i) => !!country && (
                        <MenuItem key={i} value={country.currency as string}>{country.currency}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl fullWidth sx={{ marginBottom: '1rem' }}>
                <InputLabel id="label-continent">Continent</InputLabel>
                <Select
                    labelId="label-continent"
                    id="select-conti"
                    value={code}
                    label="Continent"
                    onChange={handleChangeSelect}
                >
                    {!!dataSelect?.continents && dataSelect?.continents.map((continent, i) => !!continent && (
                        <MenuItem key={i} value={continent.code}>{continent.name}</MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl fullWidth sx={{ marginBottom: '1rem' }}>
                <Grid container spacing={2}>
                    <Grid item xs={6} md={8}>
                        <TextField
                            sx={{ display: 'grid' }}
                            id="outlined-name"
                            label="Enter Code"
                            value={text}
                            onChange={handleChange}
                        />
                    </Grid>
                    <Grid item xs={6} md={4} sx={{ display: 'flex' }}>
                        <Button
                            variant="contained"
                            onClick={() => setFilter({ code: { eq: text.toLocaleUpperCase() } })}
                            startIcon={<SearchIcon />}>
                            Search
                        </Button>
                    </Grid>
                </Grid>
            </FormControl>
            <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                {!!list?.countries &&
                    list?.countries.map(
                        (country, i) =>
                            !!country && (
                                <Grid item xs={2} sm={4} md={4} key={i}>
                                    <Card sx={{ minWidth: 275 }}>
                                        <CardContent sx={{ background: '#007fff' }}>
                                            <Typography sx={{ color: '#ffffff' }} variant="h6" gutterBottom component="div">
                                                {country.name}
                                            </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button onClick={() => navigate(`/details/${country.code}`, { replace: true })} size="small">Details</Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            ),
                    )}
            </Grid>
        </Container>
    )
}
export default CountriesList;
