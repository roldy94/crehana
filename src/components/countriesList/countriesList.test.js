import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import { CountriesList } from '../../components';
import { MemoryRouter } from 'react-router';

describe('We test the component <CountriesList />', () => {
    let wrapper = shallow(<MemoryRouter><CountriesList /></MemoryRouter>);

    beforeEach(() => {
        wrapper = shallow(<MemoryRouter><CountriesList /></MemoryRouter>);
    });
    test('should show  <CountriesList /> correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
});