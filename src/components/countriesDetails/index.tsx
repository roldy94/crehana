import React, { FC, useEffect } from 'react';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { CountriesQuery, Country } from '../../generated/graphql';
import Button from '@mui/material/Button';
import { useParams, useNavigate } from "react-router-dom";


interface Props {
    data: CountriesQuery;
}

const CountriesDetails: FC<Props> = ({ data }) => {
    let params = useParams();
    const [expanded, setExpanded] = React.useState<string | false>(false);
    const [country, setCountry] = React.useState<Country>();
    const navigate = useNavigate();
    const handleChange =
        (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
            setExpanded(isExpanded ? panel : false);
        };
    useEffect(() => {
        const filter = data.countries.filter((country) => country.code === params.code)
        setCountry(filter[0] as Country)
        // eslint-disable-next-line
    }, [country]);
    return (
        <Container sx={{ padding: '1rem' }}>
            <Button onClick={() => navigate(`/`, { replace: true })} size="small">Go Back</Button>
            <Accordion TransitionProps={{ unmountOnExit: true }} expanded={expanded === `panel_1`} onChange={handleChange(`panel_1`)}>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon sx={{ color: '#ffffff' }} />}
                    aria-controls={`panel1bh-content`}
                    id={`panel1bh-header`}
                    sx={{ background: '#007fff' }}
                >
                    <Stack spacing={2}>
                        <div style={{ alignItems: 'center' }}>
                            <Typography sx={{ color: '#ffffff' }} variant="h6" gutterBottom component="div">
                                {`${country?.code} ${country?.name}`}
                            </Typography>
                        </div>
                    </Stack>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        {country?.currency}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        {country?.continent.name}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                        {country?.capital}
                    </Typography>
                    {country?.languages.map((language, i) => (
                        <Stack spacing={2}>
                            <div style={{ alignItems: 'center' }}>
                                <Chip key={Math.random()} sx={{ marginRight: '1rem', marginBottom: '1rem' }} label={language.name} color="primary" />
                                <Chip key={Math.random()} sx={{ marginRight: '1rem', marginBottom: '1rem' }} label={language.native} color="primary" />
                            </div>
                        </Stack>
                    ))}
                </AccordionDetails>
            </Accordion>
        </Container>
    )
}

export default CountriesDetails;