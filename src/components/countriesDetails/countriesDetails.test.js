import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import { CountriesDetails } from '../../components';
import { MemoryRouter } from 'react-router';

describe('We test the component <CountriesDetails />', () => {
    let wrapper = shallow(<MemoryRouter><CountriesDetails /></MemoryRouter>);

    beforeEach(() => {
        wrapper = shallow(<MemoryRouter><CountriesDetails /></MemoryRouter>);
    });
    test('should show  <CountriesDetails /> correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
});